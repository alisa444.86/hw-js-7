/*
Document Object Model (DOM) - объектная модель документа, в которой все HTML-теги являются объектами. При этом
вложенные теги - это дети родительского элемента (тега, стоящего на порядок выше, или окружающего своих детей).
Текст внутри тегов также яаляется обїектом. Все объекты (теги и текст) доступны для JavaScript,
их можно удалять/добавлять/изменять.

*/

const array = [
    'hello', 'world', ['Kiev', 'Kharkiv', 'Odessa', 'Lviv'], 'hello', 'again', 34, 67
];

function createList(array) {
    const listContainer = document.createElement('ul');

    array.map(item => {
            if (typeof item === 'object') {
                const itemContainer = document.createElement('li');
                const innerList = document.createElement('ul');

                item.map(subitem => {
                        const innerItem = document.createElement('li');
                        innerItem.innerText = `${subitem}`;
                        innerList.appendChild(innerItem);
                    }
                );
                itemContainer.appendChild(innerList);
                listContainer.appendChild(itemContainer);
            } else {
                const itemContainer = document.createElement('li');
                itemContainer.innerText = `${item}`;
                listContainer.appendChild(itemContainer);
            }
        }
    );
    document.body.prepend(listContainer);

    let counter = 10;
    const timerContainer = document.createElement('span');
    timerContainer.innerText = `Time to delete the text is ${counter} seconds`;
    document.body.appendChild(timerContainer);

    function counterShow() {
        counter--;
        timerContainer.innerText = `Time to delete the text is ${counter} seconds`;
    }

    let timer = setInterval(counterShow, 1000);

    setTimeout(() => {
        clearInterval(timer);
        timerContainer.remove();
        listContainer.remove();
    }, 10000);
}

createList(array);