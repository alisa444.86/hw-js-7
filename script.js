
let array = ['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv'];
const array1 = [
    'hello', 'world', ['Kiev', 'Kharkiv', 'Odessa', 'Lviv'], 'hello', 'again'
];

function createList(array) {
    const listContainer = document.createElement('ul');
    document.body.prepend(listContainer);

    for (let item of array) {
        if (typeof item === 'object') {
            const itemContainer = document.createElement('li');

            /*скрытие маркировки элемента, который является вложенным списком*/
            itemContainer.style.listStyle = 'none';

            listContainer.appendChild(itemContainer);
            const innerList = document.createElement('ul');
            itemContainer.appendChild(innerList);

            for (let subitem of item) {
                const innerItem = document.createElement('li');
                innerItem.innerText = subitem;
                innerList.appendChild(innerItem);
            }
        } else {
            const itemContainer = document.createElement('li');
            itemContainer.innerText = item;
            listContainer.appendChild(itemContainer);
        }
    }

}



createList(array);
createList(array1);
